# CreateJS Culling

Implements Culling to Display Objects.

Adds a `enableCulling` property to the `createjs.DisplayObject` object. By default, the `enableCulling` property value is `true`. Before it renders the Display Object to the stage, it will run a check to determine if it needs to render. 

This does not apply to the `createjs.Shape` object.