createjs.DisplayObject.prototype.enableCulling = true;
createjs.DisplayObject.prototype.cullingBoundaries = { x: 0, y: 0, width: 0, height: 0 };

let cullingPt = {};

createjs.DisplayObject.prototype.cullingCheck = function (defaultVisible) {
	// no culling check, or if its not going to be rendered anyways, no prob!
	if (!this.enableCulling || !defaultVisible) {
		return defaultVisible;
	}

	// can't get the bounds?
	let bounds = this.getBounds();
	if (!bounds) {
		return defaultVisible;
	}

	// no parent?
	let parent = this.parent;
	if (!parent) {
		return defaultVisible;
	}

	// let's find the stage
	if (this.cullingBoundaries.width <= 0) {
		let stage = parent;

		while (parent = parent.parent) {
			stage = parent;
		}

		// set the width and height
		if (stage) {
			this.cullingBoundaries.width = stage.canvas.width;
			this.cullingBoundaries.height = stage.canvas.height;
		}
	}

	let isVisible = false;

	// top left
	this.localToGlobal(bounds.x, bounds.y, cullingPt);
	if (cullingPt.x < this.cullingBoundaries.width && cullingPt.y < this.cullingBoundaries.height) {
		// bottom right
		this.localToGlobal(bounds.x + bounds.width, bounds.y + bounds.height, cullingPt);
		if (cullingPt.x > this.cullingBoundaries.x && cullingPt.y > this.cullingBoundaries.y) {
			// it's in view
			isVisible = true;
		}
	}

	return isVisible;
};

createjs.Sprite.prototype.isVisible = function () {
	var hasContent = this.cacheCanvas || this.spriteSheet.complete;
	var visible = !!(this.visible && this.alpha > 0 && this.scaleX != 0 && this.scaleY != 0 && hasContent);
	return this.cullingCheck(visible);
};

createjs.Container.prototype.isVisible = function () {
	var hasContent = this.cacheCanvas || this.children.length;
	var visible = !!(this.visible && this.alpha > 0 && this.scaleX != 0 && this.scaleY != 0 && hasContent);
	return this.cullingCheck(visible);
};

createjs.Bitmap.prototype.isVisible = function () {
	var image = this.image;
	var hasContent = this.cacheCanvas || (image && (image.naturalWidth || image.getContext || image.readyState >= 2));
	var visible = !!(this.visible && this.alpha > 0 && this.scaleX != 0 && this.scaleY != 0 && hasContent);
	return this.cullingCheck(visible);
};

createjs.Text.prototype.isVisible = function () {
	var hasContent = this.cacheCanvas || (this.text != null && this.text !== "");
	var visible = !!(this.visible && this.alpha > 0 && this.scaleX != 0 && this.scaleY != 0 && hasContent);
	return this.cullingCheck(visible);
};
